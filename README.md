# Generate Random Number

This project generate cryptographically strong random number using crypto-random module.

## Deploy
1. Run below command to install required modules.
```node.JS
npm i
```
2. Save and run the code in VScode editor.
```node.JS
node app.js
```
